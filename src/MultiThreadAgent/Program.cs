﻿using Game;
using Game.Commands;
using Game.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Information()
    .Enrich.FromLogContext()
    .WriteTo.File("AppLog.log")
    .WriteTo.Console()
    .MinimumLevel.Debug()
    .CreateLogger();

var hostBuilder = Host
    .CreateDefaultBuilder(args)
    .ConfigureLogging(
        logging =>
        {
            logging.ClearProviders();
            logging.AddSerilog();
        })
    .ConfigureServices((h,services) =>
    {
        services.AddSingleton<Program>();
    });

var host = hostBuilder.Build();
host.Services.GetRequiredService<Program>().Run(args);

public partial class Program
{
    private readonly ILogger<Program> _logger;

    public Program(
        ILogger<Program> logger)
    {
         _logger = logger;
    }

    private void Run(string[] args)
    {
        var logger = new ConsoleLogger();

        var server = new MultiThreadGameServer(2, logger);
        server.InitChildQueues();
        server.Run();

        server.ScheduleCommand(new LoggingCommand("Command 1", logger));
        server.ScheduleCommand(new LoggingCommand("Command 2", logger));
        server.ScheduleCommand(new LoggingCommand("Command 3", logger));
        server.ScheduleCommand(new LoggingCommand("Command 4", logger));
        server.ScheduleCommand(new LoggingCommand("Command 5", logger));
        server.ScheduleCommand(new LoggingCommand("Command 6", logger));
        server.ScheduleCommand(new LoggingCommand("Command 7", logger));
        server.ScheduleCommand(new LoggingCommand("Command 8", logger));
        server.ScheduleCommand(new LoggingCommand("Command 9", logger));

        server.SoftStopChildThreads();
        //server.HardStopChildThreads();

        server.SoftStop();
        //server.HardStop();

        server.ScheduleCommand(new LoggingCommand("Command 10", logger));
        server.ScheduleCommand(new LoggingCommand("Command 11", logger));
        server.ScheduleCommand(new LoggingCommand("Command 12", logger));
        server.ScheduleCommand(new LoggingCommand("Command 13", logger));
        server.ScheduleCommand(new LoggingCommand("Command 14", logger));
        server.ScheduleCommand(new LoggingCommand("Command 15", logger));
    }
}

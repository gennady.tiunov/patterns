﻿using Game.Movement;

namespace Game.GameObjects;

public class SpaceShip : IMovable, IRotable
{
    private Vector _position;
    private Vector _velocity;
    
    private int _angularDirection;
    private int _angularVelocity;
    private int _angularDirectionsCount;

    public SpaceShip(
        Vector position,
        Vector velocity,
        int angularDirection,
        int angularVelocity,
        int angularDirectionsCount)
    {
        _position = position;
        _velocity = velocity;
        _angularDirection = angularDirection;
        _angularVelocity = angularVelocity;
        _angularDirectionsCount = angularDirectionsCount;
    }

    public Vector GetPosition()
    {
        return _position;
    }

    public Vector GetVelocity()
    {
        return _velocity;
    }

    public void SetPosition(Vector position)
    {
        _position = position;
    }

    public void SetVelocity(Vector velocity)
    {
        _velocity = velocity;
    }

    public int GetAngularDirection()
    {
        return _angularDirection;
    }

    public int GetAngularVelocity()
    {
        return _angularVelocity;
    }

    public void SetAngularDirection(int direction)
    {
        _angularDirection = direction;
    }

    public int GetAngularDirectionsCount()
    {
        return _angularDirectionsCount;
    }
}
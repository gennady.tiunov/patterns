﻿namespace Game.Infrastructure;

public interface IGameLogger
{
    public void Log(string message);
}
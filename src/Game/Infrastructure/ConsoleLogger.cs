﻿namespace Game.Infrastructure;

public class ConsoleLogger : IGameLogger
{
    public void Log(string message)
    {
        Console.WriteLine(message);
    }
}
﻿using System.Collections.Concurrent;
using Game.Commands;
using Game.Infrastructure;

namespace Game;

public class MultiThreadGameServer
{
    public BlockingCollection<ICommand> ServerQueue { get; } = new ();

    private readonly ServerThread _serverThread;
    private readonly ServerThread[] _childThreads;

    private readonly IGameLogger _gameLogger;

    public MultiThreadGameServer(
        int childQueueCount,
        IGameLogger gameLogger)
    {
        _serverThread = new ServerThread(
            new Thread(ListenToQueueInServerThread),
            ServerQueue,
            gameLogger);

        _childThreads = new ServerThread[childQueueCount];
        for (var i = 0; i < childQueueCount; i++)
        {
            _childThreads[i] = new ServerThread(
                new Thread(ListenToQueueInServerThread),
                ServerQueue,
                gameLogger);
        }

        _gameLogger = gameLogger;
    }

    public void ScheduleCommand(ICommand command)
    {
        ServerQueue.Add(command);
    }

    public void InitChildQueues()
    {
        foreach (var thread in _childThreads)
        {
            _gameLogger.Log($"Scheduling StartThreadProcessingCommand for thread {thread.Id}");
            ScheduleCommand(new StartThreadProcessingCommand(thread));
        }
    }

    public void HardStopChildThreads()
    {
        foreach (var thread in _childThreads)
        {
            _gameLogger.Log($"Scheduling HardStopThreadProcessingCommand for thread {thread.Id}");
            ScheduleCommand(new HardStopThreadProcessingCommand(thread));
        }
    }

    public void SoftStopChildThreads()
    {
        foreach (var thread in _childThreads)
        {
            _gameLogger.Log($"Scheduling SoftStopThreadProcessingCommand for thread {thread.Id}");
            ScheduleCommand(new SoftStopThreadProcessingCommand(thread));
        }
    }

    public void Run()
    {
        Console.Clear();

        _serverThread.Start();
    }

    public void HardStop()
    {
        _serverThread.HardStop();
    }

    public void SoftStop()
    {
        _serverThread.SoftStop();
    }

    private void ListenToQueueInServerThread(object? parameters)
    {
        var serverThread = (ServerThread) parameters!;

        _gameLogger.Log($"Listening to command queue in thread {serverThread.Id}: started");

        while (!serverThread.CheckStoppingBehaviour())
        {
            if (serverThread.CommandQueue.TryTake(out var command))
            {
                try
                {
                    var message = $"Processing command '{command.GetType().Name}' in thread {serverThread.Id}";
                    _gameLogger.Log(message);

                    command.Execute();
                }
                catch (Exception e)
                {
                    _gameLogger.Log(
                        $"Exception '{nameof(e)}' ('{e.Message}') occurred in thread {serverThread.Id}");
                }
            }
        }

        _gameLogger.Log($"Listening to command queue in thread {serverThread.Id}: finished");
    }
}

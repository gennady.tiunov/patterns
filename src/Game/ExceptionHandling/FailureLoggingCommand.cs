﻿using Game.Commands;
using Game.Infrastructure;

namespace Game.ExceptionHandling;

public class FailureLoggingCommand : ICommand
{
    private readonly ICommand _command;
    private readonly Exception _exception;
    private readonly IGameLogger _gameLogger;
    
    public FailureLoggingCommand(
        ICommand command,
        Exception exception,
        IGameLogger gameLogger)
    {
        _command = command;
        _exception = exception;
        _gameLogger = gameLogger;
    }
    
    public void Execute()
    {
        _gameLogger.Log($"{GetType().Name}: Exception '{_exception.GetType().Name}' occured while executing command '{_command.GetType().Name}': '{_exception.Message}'");
    }
}
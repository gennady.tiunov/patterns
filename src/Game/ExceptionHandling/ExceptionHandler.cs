﻿using Game.Commands;

namespace Game.ExceptionHandling;

public class ExceptionHandler
{
    private readonly Dictionary<Type, Action<ICommand, Exception>> _commonHandlers = new ();
    
    private readonly Dictionary<Type, Dictionary<Type, Action<ICommand, Exception>>> _specificHandlers = new();
    
    public void RegisterHandler<TCommand, TException>(
       Action<ICommand, Exception> handler)
        where TCommand : ICommand
        where TException : Exception
    {
        if (!_specificHandlers.ContainsKey(typeof(TCommand)))
        {
            _specificHandlers[typeof(TCommand)] = new Dictionary<Type, Action<ICommand, Exception>>();
        }

        _specificHandlers[typeof(TCommand)][typeof(TException)] = handler;
    }
    
    public void RegisterHandler<TCommand>(
        Action<ICommand, Exception> handler)
    {
        _commonHandlers[typeof(TCommand)] = handler;
    }

    public void HandleException(
        ICommand command,
        Exception exception)
    {
        if (_specificHandlers.ContainsKey(command.GetType()) &&
            _specificHandlers[command.GetType()].ContainsKey(exception.GetType()))
        {
            _specificHandlers[command.GetType()][exception.GetType()](command, exception);
            return;
        }

        if (_commonHandlers.ContainsKey(command.GetType()))
        {
            _commonHandlers[command.GetType()](command, exception);
            return;
        }

        throw new InvalidOperationException($"No handler found for command '{command.GetType()}' and exception '{exception.GetType()}'");
    }
}
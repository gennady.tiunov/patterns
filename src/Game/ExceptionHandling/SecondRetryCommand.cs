﻿using Game.Commands;
using Game.Infrastructure;

namespace Game.ExceptionHandling;

public class SecondRetryCommand : ICommand
{
    public ICommand InnerCommand { get; }

    private readonly IGameLogger _logger;
    
    public SecondRetryCommand(
        ICommand command,
        IGameLogger logger)
    {
        InnerCommand = command;
        _logger = logger;
    }
    
    public void Execute()
    {
        _logger.Log($"{GetType().Name}: Retrying command '{InnerCommand.GetType()}'");
            
        InnerCommand.Execute();
    }
}
﻿using Game.Commands;
using Game.Infrastructure;

namespace Game.ExceptionHandling;

public class ExactCountRetryCommand : ICommand
{
    private readonly ICommand _command;
    private readonly int _maxAttempts;
    
    private readonly Queue<ICommand> _commandQueue;
    private readonly IGameLogger _logger;
    
    private int _currentAttempt;

    public ExactCountRetryCommand(
        ICommand command,
        int maxAttempts,
        Queue<ICommand> commandQueue,
        IGameLogger logger)
    {
        _command = command;
        _maxAttempts = maxAttempts;
        _commandQueue = commandQueue;
        _logger = logger;
    }
    
    public void Execute()
    {
        _currentAttempt++;

        try
        {
            _logger.Log($"{GetType().Name}: Executing command '{_command.GetType()}', retry attempt: {_currentAttempt}");
            
            _command.Execute();
        }
        catch (Exception exception)
        {
            if (_currentAttempt < _maxAttempts)
            {
                _commandQueue.Enqueue(this);
            }
            else
            {
                _commandQueue.Enqueue(new FailureLoggingCommand(_command, exception, _logger));
            }
        }
    }
}
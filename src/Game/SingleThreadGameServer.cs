﻿using Game.Commands;
using Game.ExceptionHandling;
using Game.Infrastructure;

namespace Game;

public class SingleThreadGameServer
{
    private Queue<ICommand> _commandQueue = new();
    private ExceptionHandler _exceptionHandler;
    private IGameLogger _gameLogger;

    public SingleThreadGameServer(
        ExceptionHandler exceptionHandler,
        IGameLogger gameLogger)
    {
        _exceptionHandler = exceptionHandler;
        _gameLogger = gameLogger;
    }

    public Queue<ICommand> CommandQueue => _commandQueue;

    public void ScheduleCommand(ICommand command)
    {
        _commandQueue.Enqueue(command);
    }

    public void Run()
    {
        Console.Clear();

        while (_commandQueue.TryDequeue(out var command))
        {
            _gameLogger.Log($"{GetType().Name}: Processing command '{command.GetType().Name}'");

            Thread.Sleep(1000);

            try
            {
                command.Execute();
            }
            catch (Exception exception)
            {
                _exceptionHandler.HandleException(command, exception);
            }
        }
    }
}

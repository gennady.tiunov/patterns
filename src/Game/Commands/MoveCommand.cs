﻿using Game.Movement;

namespace Game.Commands;

public class MoveCommand : ICommand
{
    private readonly IMovable _movable;

    public MoveCommand(IMovable movable)
    {
        _movable = movable;
    }

    public void Execute()
    {
        _movable.SetPosition(Vector.Plus(_movable.GetPosition(), _movable.GetVelocity()));
    }
}
﻿namespace Game.Commands;

public class ExceptionCommand : ICommand
{
    private readonly Exception _exception;

    public ExceptionCommand(Exception exception)
    {
        _exception = exception;
    }

    public void Execute()
    {
        throw _exception;
    }
}
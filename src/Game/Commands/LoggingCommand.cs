﻿using Game.Infrastructure;

namespace Game.Commands;

public class LoggingCommand : ICommand
{
    private readonly string _message;
    private readonly IGameLogger _gameLogger;

    public LoggingCommand(
        string message,
        IGameLogger gameLogger)
    {
        _message = message;
        _gameLogger = gameLogger;
    }

    public void Execute()
    {
        _gameLogger.Log(_message);
    }
}

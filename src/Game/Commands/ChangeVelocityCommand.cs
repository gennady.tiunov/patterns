﻿using Game.Movement;

namespace Game.Commands;

public class ChangeVelocityCommand : ICommand
{
    private readonly IRotable _rotable;
    private readonly IMovable _movable;

    public ChangeVelocityCommand(
        IMovable movable,
        IRotable rotable)
    {
        _movable = movable;
        _rotable = rotable;
    }

    public void Execute()
    {
        var velocity = _movable.GetVelocity();
        
        var angularVelocity = _rotable.GetAngularVelocity();
        var angularDirectionsCount = _rotable.GetAngularDirectionsCount();
        
        var newVelocityX = (int) (velocity.X * Math.Cos(angularVelocity / (double)(angularDirectionsCount * 360))
                                  - velocity.Y * Math.Sin(angularVelocity / (double)(angularDirectionsCount * 360)));  
        
        var newVelocityY = (int) (velocity.X * Math.Sin(angularVelocity / (double)(angularDirectionsCount * 360))
                                  + velocity.Y * Math.Cos(angularVelocity / (double)(angularDirectionsCount * 360)));

        _movable.SetVelocity(new Vector(newVelocityX, newVelocityY));
    }
}
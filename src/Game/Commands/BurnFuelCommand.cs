﻿using Game.Movement;

namespace Game.Commands;

public class BurnFuelCommand : ICommand
{
    private readonly IFuelPowered _fuelPowered;

    public BurnFuelCommand(IFuelPowered fuelPowered)
    {
        _fuelPowered = fuelPowered;
    }

    public void Execute()
    {
        var currentFuelLevel = _fuelPowered.GetFuelLevel();

        var fuelRemains = currentFuelLevel - _fuelPowered.BurningVelocity;
        
        _fuelPowered.SetFuelLevel(fuelRemains);
    }
}
﻿namespace Game.Commands;

public class HardStopThreadProcessingCommand : ICommand
{
    private readonly ServerThread _serverThread;

    public HardStopThreadProcessingCommand(ServerThread serverThread)
    {
        _serverThread = serverThread;
    }

    public void Execute()
    {
        _serverThread.HardStop();
    }
}

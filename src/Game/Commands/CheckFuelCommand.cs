﻿using Game.Movement;

namespace Game.Commands;

public class CheckFuelCommand : ICommand
{
    private readonly IFuelPowered _fuelPowered;

    public CheckFuelCommand(IFuelPowered fuelPowered)
    {
        _fuelPowered = fuelPowered;
    }

    public void Execute()
    {
        var currentFuelLevel = _fuelPowered.GetFuelLevel();

        var fuelRemains = currentFuelLevel - _fuelPowered.BurningVelocity;

        if (fuelRemains < 0)
        {
            throw new CommandException("Not enough fuel to move!");
        }
    }
}
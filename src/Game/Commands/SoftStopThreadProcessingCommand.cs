﻿namespace Game.Commands;

public class SoftStopThreadProcessingCommand : ICommand
{
    private readonly ServerThread _serverThread;

    public SoftStopThreadProcessingCommand(ServerThread serverThread)
    {
        _serverThread = serverThread;
    }

    public void Execute()
    {
        _serverThread.SoftStop();
    }
}

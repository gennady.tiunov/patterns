﻿namespace Game.Commands;

public class StartThreadProcessingCommand : ICommand
{
    private readonly ServerThread _serverThread;

    public StartThreadProcessingCommand(ServerThread serverThread)
    {
        _serverThread = serverThread;
    }

    public void Execute()
    {
        _serverThread.Start();
    }
}

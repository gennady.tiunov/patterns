﻿using Game.Movement;

namespace Game.Commands;

public class RotateCommand : ICommand
{
    private readonly IRotable _rotable;

    public RotateCommand(IRotable rotable)
    {
        _rotable = rotable;
    }

    public void Execute()
    {
        var direction = (_rotable.GetAngularDirection() + _rotable.GetAngularVelocity()) % _rotable.GetAngularDirectionsCount();
        
        _rotable.SetAngularDirection(direction);
    }
}
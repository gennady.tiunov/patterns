﻿namespace Game.Movement;

public record Vector(int X, int Y)
{
    public static Vector Plus(Vector position, Vector velocity)
    {
        return new Vector(position.X + velocity.X, position.Y + velocity.Y);
    }
}
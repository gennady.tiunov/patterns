﻿namespace Game.Movement;

public interface IFuelPowered
{
    int BurningVelocity { get; }
    
    int GetFuelLevel();

    void SetFuelLevel(int fuelLevel);
}
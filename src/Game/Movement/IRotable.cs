﻿namespace Game.Movement;

public interface IRotable
{
    int GetAngularDirection();
    
    int GetAngularVelocity();
    
    void SetAngularDirection(int direction);
    
    int GetAngularDirectionsCount();
 }
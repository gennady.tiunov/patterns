﻿namespace Game.Movement;

public interface IMovable
{
    Vector GetPosition();
    
    Vector GetVelocity();
    
    void SetPosition(Vector position);
    
    void SetVelocity(Vector velocity);
}
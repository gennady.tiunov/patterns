﻿using System.Collections.Concurrent;
using Game.Commands;
using Game.Infrastructure;
namespace Game;

public class ServerThread
{
    private Thread Thread { get; }

    public BlockingCollection<ICommand> CommandQueue { get; }

    public Func<bool> CheckStoppingBehaviour { get; private set; }

    private IGameLogger Logger{ get; }

    public ServerThread(
        Thread thread,
        BlockingCollection<ICommand> commandQueue,
        IGameLogger logger)
    {
        Thread = thread;
        CommandQueue = commandQueue;
        CheckStoppingBehaviour = NeverStopCheckBehaviour;
        Logger = logger;
    }

    public int Id => Thread.ManagedThreadId;

    public bool IsRunning => Thread.IsAlive;

    public void Start()
    {
        Thread.Start(this);
        Logger.Log($"Thread {Id} started.");
    }

    public void HardStop()
    {
        SetStoppingBehaviour(HardStopCheckBehaviour);
        Logger.Log($"HardStopCheckBehaviour set to thread {Id}.");
    }

    public void SoftStop()
    {
        SetStoppingBehaviour(SoftStopCheckBehaviour);
        Logger.Log($"SoftStopCheckBehaviour set to thread {Id}.");
    }

    private bool NeverStopCheckBehaviour()
    {
        return false;
    }

    private bool HardStopCheckBehaviour()
    {
        return true;
    }

    private bool SoftStopCheckBehaviour()
    {
        return !CommandQueue.Any();
    }

    private void SetStoppingBehaviour(Func<bool> behaviour)
    {
        CheckStoppingBehaviour = behaviour;
    }
}

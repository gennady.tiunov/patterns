﻿using System;

namespace QuadraticEquations;

public static class QuadraticEquationSolver
{
    public static double[] Solve(double a, double b, double c)
    {
        if (IsDoubleNumberZero(a))
        {
            throw new ArgumentOutOfRangeException(nameof(a));
        }

        AssertCoefficientIsCalculable(nameof(a), a);
        AssertCoefficientIsCalculable(nameof(b), b);
        AssertCoefficientIsCalculable(nameof(c), c);

        if (IsDoubleNumberZero(b))
        {
            if (c > 0)
            {
                return Array.Empty<double>();
            }

            if (c < 0)
            {
                return new[]
                {
                    Math.Sqrt(-c),
                    -Math.Sqrt(-c)
                };
            }
        }
        
        var discriminant = b * b - 4 * a * c;
        if (discriminant < 0)
        {
            return Array.Empty<double>();
        }
        if (IsDoubleNumberZero(discriminant))
        {
            return new[] { -b / (2 * a) };
        }
        
        return new[] { (-b + discriminant) / (2 * a), (-b - discriminant) / (2 * a) };
    }
    
    private static bool IsDoubleNumberZero(double number)
    {
        const double epsilon = 0.000_000_001;
        
        return Math.Abs(number - 0) <= epsilon;
    }

    private static void AssertCoefficientIsCalculable(string coefficientName, double coefficientValue)
    {
        if (double.IsNaN(coefficientValue) || double.IsInfinity(coefficientValue))
        {
            throw new ArgumentOutOfRangeException(
                message: $"Coefficient has an invalid value (coefficient: name: '{coefficientName}',  value: '{coefficientValue}')",
                innerException: null);
        }
    }
}
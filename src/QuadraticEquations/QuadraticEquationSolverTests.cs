﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace QuadraticEquations;

public static class UnitTests
{
    [Fact]
    public static void Solve_AIsOneAndBIsZeroAndCIsOne_EquationShouldHaveNoRoots()
    {
        // arrange
        double a = 1.0;
        double b = 0.0;
        double c = 1.0;
        
        // act
        var result = QuadraticEquationSolver.Solve(a, b, c);
        
        // assert
        result.Should().HaveCount(0);
    }
    
    [Fact]
    public static void Solve_AIsOneAndBIsZeroAndCIsMinusOne_EquationShouldHaveTwoRoots()
    {
        // arrange
        double a = 1.0;
        double b = 0.0;
        double c = -1.0;
        
        // act
        var result = QuadraticEquationSolver.Solve(a, b, c);
        
        // assert
        result.Should().HaveCount(2);
        result.First().Should().Be(1);
        result.Skip(1).Take(1).Single().Should().Be(-1);
    }
    
    [Fact]
    public static void Solve_DiscriminantIsZero_EquationShouldHaveOneRoot()
    {
        // arrange
        double a = 1.0;
        double b = 2.0;
        double c = 1.0;
        
        // act
        var result = QuadraticEquationSolver.Solve(a, b, c);
        
        // assert
        result.Should().HaveCount(1);
        result.Single().Should().Be(-1);
    }
    
    [Fact]
    public static void Solve_DiscriminantIsSlightlyAboveZero_EquationShouldHaveTwoRoots()
    {
        // arrange
        double a = 1.0;
        double b = 2.0;
        double c = 0.999_999_999;
        
        // act
        var result = QuadraticEquationSolver.Solve(a, b, c);
        
        // assert
        result.Should().HaveCount(2);
    }

    [Fact] public static void Solve_DiscriminantIsLessThanEpsilon_EquationShouldHaveOneRoot()
    {
        // arrange
        double a = 1.0;
        double b = 2.0;
        double c = 0.999_999_9999;
        
        // act
        var result = QuadraticEquationSolver.Solve(a, b, c);
        
        // assert
        result.Should().HaveCount(1);
        result.Single().Should().Be(-1);
    }
    
    [Fact]
    public static void Solve_BIsNotZeroAndCIsNegative_EquationShouldHaveOneRoot()
    {
        // arrange
        double a = 1.0;
        double b = 2.0;
        double c = 1.0;
        
        // act
        var result = QuadraticEquationSolver.Solve(a, b, c);
        
        // assert
        result.Should().HaveCount(1);
        result.Single().Should().Be(-1);
    }
    
    [Fact]
    public static void Solve_AIsZero_ShouldThrowArgumentOutOfRangeException()
    {
        // arrange
        double a = 0.0;
        double b = new Random().NextDouble();
        double c = new Random().NextDouble();
        
        // act
        var action = () => QuadraticEquationSolver.Solve(a, b, c);;
        
        // assert
        action.Should()
            .ThrowExactly<ArgumentOutOfRangeException>()
            .WithMessage("Specified argument was out of the range of valid values. (Parameter 'a')");
    }
    
    [Fact]
    public static void Solve_AIsSlightlyAboveZero_ShouldNotThrowArgumentOutOfRangeException()
    {
        // arrange
        double a = 0.000_000_01;
        double b = new Random().NextDouble();
        double c = new Random().NextDouble();
        
        // act
        var action = () => QuadraticEquationSolver.Solve(a, b, c);;
        
        // assert
        action.Should().NotThrow();
    }
    
    [Fact]
    public static void Solve_AIsEpsilon_ShouldThrowArgumentOutOfRangeException()
    {
        // arrange
        double a = 0.000_000_001;
        double b = new Random().NextDouble();
        double c = new Random().NextDouble();
        
        // act
        var action = () => QuadraticEquationSolver.Solve(a, b, c);;
        
        // assert
        action.Should()
            .ThrowExactly<ArgumentOutOfRangeException>()
            .WithMessage("Specified argument was out of the range of valid values. (Parameter 'a')");
    }
    
    [Theory]
    [MemberData(nameof(GetInvalidDoubleValues))]
    public static void Solve_AIsInvalidNumber_ThrowArgumentOutOfRangeException(double a)
    {
        // arrange
        double b = new Random().NextDouble();
        double c = new Random().NextDouble();
        
        // act
        var action = () => QuadraticEquationSolver.Solve(a, b, c);;
        
        // assert
        action.Should()
            .ThrowExactly<ArgumentOutOfRangeException>()
            .WithMessage("Coefficient has an invalid value *");
    }
    
    [Theory]
    [MemberData(nameof(GetInvalidDoubleValues))]
    public static void Solve_BIsInvalidNumber_ThrowArgumentOutOfRangeException(double b)
    {
        // arrange
        double a = new Random().NextDouble();
        double c = new Random().NextDouble();
        
        // act
        var action = () => QuadraticEquationSolver.Solve(a, b, c);;
        
        // assert
        action.Should()
            .ThrowExactly<ArgumentOutOfRangeException>()
            .WithMessage("Coefficient has an invalid value *");
    }
    
    [Theory]
    [MemberData(nameof(GetInvalidDoubleValues))]
    public static void Solve_CIsInvalidNumber_ThrowArgumentOutOfRangeException(double c)
    {
        // arrange
        double a = new Random().NextDouble();
        double b = new Random().NextDouble();
        
        // act
        var action = () => QuadraticEquationSolver.Solve(a, b, c);;
        
        // assert
        action.Should()
            .ThrowExactly<ArgumentOutOfRangeException>()
            .WithMessage("Coefficient has an invalid value *");
    }

    public static IEnumerable<object[]> GetInvalidDoubleValues()
    {
        return  new object[]
        {
            double.NaN,
            double.NegativeInfinity,
            double.PositiveInfinity
        }.Select(x => new [] { x });
    }
}
﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;

namespace Sorting.Implementation.Helpers;

public static class ArrayFileHelpers<T>
{
    public static T[] ReadArray(string filePath, char separator)
    {
        TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));
        
        return File
            .ReadAllText(filePath)
            .Split(separator)
            .Select(x => (T)converter.ConvertFromString(x)!)
            .ToArray();
    }
    
    public static void WriteArray(T[] array, string filePath, char separator)
    {
        var text = String.Join(separator, array);
        
        File.WriteAllText(filePath, text);
    }
}
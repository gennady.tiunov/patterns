﻿using System;
using Sorting.Abstractions;
using Sorting.Implementation.Helpers;

namespace Sorting.Implementation.Merge;

public class StringMergeSorter : AbstractMergeSorter
{
    public override void Sort(string originalFilePath, string resultFilePath)
    {
        var array = ArrayFileHelpers<string>.ReadArray(originalFilePath, ',');
        
        Array.Sort(array);
        
        ArrayFileHelpers<string>.WriteArray(array, resultFilePath, ',');    }
}
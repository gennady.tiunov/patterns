﻿using System;
using Sorting.Abstractions;
using Sorting.Implementation.Helpers;

namespace Sorting.Implementation.Selection;

public class StringSelectionSorter : AbstractSelectionSorter
{
    public override void Sort(string originalFilePath, string resultFilePath)
    {
        var array = ArrayFileHelpers<string>.ReadArray(originalFilePath, ',');
        
        Array.Sort(array);
        
        ArrayFileHelpers<string>.WriteArray(array, resultFilePath, ',');    }
}
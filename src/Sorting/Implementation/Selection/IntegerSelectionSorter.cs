﻿using System;
using Sorting.Abstractions;
using Sorting.Implementation.Helpers;

namespace Sorting.Implementation.Selection;

public class IntegerSelectionSorter : AbstractSelectionSorter
{
    public override void Sort(string originalFilePath, string resultFilePath)
    {
        var array = ArrayFileHelpers<int>.ReadArray(originalFilePath, ',');
        
        Array.Sort(array);
        
        ArrayFileHelpers<int>.WriteArray(array, resultFilePath, ',');    }
}
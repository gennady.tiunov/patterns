﻿using System;
using Sorting.Abstractions;

namespace Sorting.Implementation.Sorting;

public class Sorter
{
   private readonly AbstractSortingFactory _sortingFactory;
   
   public Sorter(
      AbstractSortingFactory sortingFactory)
   {
      _sortingFactory = sortingFactory;
   }

   public void Sort(
      string originalFilePath,
      string resultFilePath,
      SortingMethod sortingMethod)
   {
      AbstractSorter sorter;

      switch (sortingMethod)
      {
         case SortingMethod.Insertion:
            sorter = _sortingFactory.CreateInsertionSorter();
            break;
         
         case SortingMethod.Selection:
            sorter = _sortingFactory.CreateSelectionSorter();
            break;
         
         case SortingMethod.Merge:
            sorter = _sortingFactory.CreateMergeSorter();
            break;
         
         default:
            throw new ArgumentOutOfRangeException(nameof(sortingMethod));
      }
      
      sorter.Sort(originalFilePath, resultFilePath);
   }
}
﻿namespace Sorting.Implementation.Sorting;

public enum SortingDirection
{
    Asc,
    Desc
}

﻿namespace Sorting.Implementation.Sorting;

public enum ElementDataType
{
    Integer,
    String,
}

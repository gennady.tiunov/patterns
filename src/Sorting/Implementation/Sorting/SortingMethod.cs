﻿namespace Sorting.Implementation.Sorting;

public enum SortingMethod
{
    Insertion,
    Selection,
    Merge
}

﻿using Sorting.Abstractions;
using Sorting.Implementation.Insertion;
using Sorting.Implementation.Merge;
using Sorting.Implementation.Selection;

namespace Sorting.Implementation.Factories;

public class StringSortingFactory : AbstractSortingFactory
{
	public override AbstractInsertionSorter CreateInsertionSorter()
	{
		return new StringInsertionSorter();
	}

	public override AbstractSelectionSorter CreateSelectionSorter()
	{
		return new StringSelectionSorter();
	}

	public override AbstractMergeSorter CreateMergeSorter()
	{
		return new StringMergeSorter();
	}
}	
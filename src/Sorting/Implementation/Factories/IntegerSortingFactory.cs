﻿using Sorting.Abstractions;
using Sorting.Implementation.Insertion;
using Sorting.Implementation.Merge;
using Sorting.Implementation.Selection;

namespace Sorting.Implementation.Factories;

public class IntegerSortingFactory : AbstractSortingFactory
{
	public override AbstractInsertionSorter CreateInsertionSorter()
	{
		return new IntegerInsertionSorter();
	}

	public override AbstractSelectionSorter CreateSelectionSorter()
	{
		return new IntegerSelectionSorter();
	}

	public override AbstractMergeSorter CreateMergeSorter()
	{
		return new IntegerMergeSorter();
	}
}	
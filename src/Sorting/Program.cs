﻿using System;
using CommandLine;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Sorting.Abstractions;
using Sorting.CommandLine;
using Sorting.Implementation.Factories;
using Sorting.Implementation.Sorting;

var hostBuilder = Host
    .CreateDefaultBuilder(args)
    .ConfigureLogging(
        logging =>
        {
            logging.ClearProviders();
        })
    .ConfigureServices((h,services) =>
    {
        services.AddSingleton<Program>();
    });

var host = hostBuilder.Build();
host.Services.GetRequiredService<Program>().Run(args);

public partial class Program
{
    private void Run(string[] args)
    {
        Console.Clear();
        
        Parser.Default.ParseArguments<Parameters>(args)
            .WithParsed(parameters =>
            {
                AbstractSortingFactory sortingFactory = parameters.ElementDataType switch
                {
                    ElementDataType.Integer => new IntegerSortingFactory(),
                    ElementDataType.String => new StringSortingFactory(),
                    _ => throw new ArgumentOutOfRangeException(nameof(parameters.ElementDataType))
                };

                var sorter = new Sorter(sortingFactory);
                sorter.Sort(parameters.OriginalFilePath, parameters.ResultFilePath, parameters.SortingMethod);
        
                Console.WriteLine("Press any key...");
                Console.ReadKey();
            });
    }
}
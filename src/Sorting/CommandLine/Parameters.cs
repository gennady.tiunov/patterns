﻿using CommandLine;
using Sorting.Implementation.Sorting;

namespace Sorting.CommandLine
{
    public class Parameters
    {
        [Option(
            "original-file-path",
            Required = true,
            HelpText = "Path of file with array to sort.")]
        public string OriginalFilePath { get; set; } = null!;
        
        [Option(
            "result-file-path",
            Required = true,
            HelpText = "Path of file to save results to.")]
        public string ResultFilePath { get; set; } = null!;

        [Option(
            "sorting-method",
            Required = true,
            HelpText = "Sorting method (Insertion, Selection, Merge).")]
        public SortingMethod SortingMethod { get; set; }
        
        [Option(
            "element-data-type",
            Required = true,
            HelpText = "Data type of elements in file to sort (Integer, String).")]
        public ElementDataType ElementDataType { get; set; }
    }
}
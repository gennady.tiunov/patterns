﻿namespace Sorting.Abstractions;

public abstract class AbstractSortingFactory
{
	public abstract AbstractInsertionSorter CreateInsertionSorter();

	public abstract AbstractSelectionSorter CreateSelectionSorter();
	
	public abstract AbstractMergeSorter CreateMergeSorter();
}	
﻿namespace Sorting.Abstractions;

public abstract class AbstractSorter
{
    public abstract void Sort(string originalFilePath, string resultFilePath);
}
﻿namespace IoC;

public static class IoC
{
    private static readonly Dictionary<Type, Func<Type>> Dependencies = new();
    
    public static void Register<T>()
    {
        Dependencies[typeof(T)] = () => typeof(T);
    }

    public static void Register<TInterface>(Type implementationType)
    {
        Dependencies[typeof(TInterface)] = () => implementationType;
    }
    
    public static void Register<TInterface>(Func<Type> implementationType)
    {
        Dependencies[typeof(TInterface)] = implementationType;
    }
    
    public static T Resolve<T>(params object[] parameters)
    {
        return (T) Activator.CreateInstance(Dependencies[typeof(T)](), parameters)!;
    }
}
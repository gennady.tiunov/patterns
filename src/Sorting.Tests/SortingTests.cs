﻿using FluentAssertions;
using Sorting.Implementation.Factories;
using Sorting.Implementation.Sorting;
using Xunit;

namespace Sorting.Tests;

public class SortingFixture
{
    const string FilesDirectory = "Files";
    
    const string FileRandomIntegers = "RandomIntegers.txt";
    const string FileNameRandomIntegersResults = "RandomIntegers-SortedResult.txt";
    const string FileNameRandomIntegersSample = "SortedIntegers-Sample.txt";
    const string FileNameRandomStrings = "RandomStrings.txt";
    const string FileNameRandomStringsResults = "RandomStrings-SortedResult.txt";
    const string FileNameRandomStringsSample = "SortedStrings-Sample.txt";
    
    public readonly string FilePathRandomIntegers = Path.Join(FilesDirectory, FileRandomIntegers);
    public readonly string FilePathRandomIntegersResults = Path.Join(FilesDirectory, FileNameRandomIntegersResults);
    public readonly string FilePathRandomIntegersSample = Path.Join(FilesDirectory, FileNameRandomIntegersSample);
    public readonly string FilePathRandomStrings = Path.Join(FilesDirectory, FileNameRandomStrings);
    public readonly string FilePathRandomStringsResults = Path.Join(FilesDirectory, FileNameRandomStringsResults);
    public readonly string FilePathRandomStringsSample = Path.Join(FilesDirectory, FileNameRandomStringsSample);
}

public class SortingTests : IClassFixture<SortingFixture>
{
    private readonly SortingFixture _fixture;

    public SortingTests(SortingFixture fixture)
    {
        _fixture = fixture;
    }
    
    [Fact]
    public void Sort_Integers_InsertionMethod_ResultFileCoincidesSample()
    {
        // arrange
        var sorter = new Sorter(new IntegerSortingFactory());

        // act
        sorter.Sort(_fixture.FilePathRandomIntegers, _fixture.FilePathRandomIntegersResults, SortingMethod.Insertion);

        // assert
        var resultContents = File.ReadAllText(_fixture.FilePathRandomIntegersResults);
        var sampleContents = File.ReadAllText(_fixture.FilePathRandomIntegersSample);

        resultContents.Should().Match(sampleContents);
    }
    
    [Fact]
    public void Sort_Integers_SelectionMethod_ResultFileCoincidesSample()
    {
        // arrange
        var sorter = new Sorter(new IntegerSortingFactory());

        // act
        sorter.Sort(_fixture.FilePathRandomIntegers, _fixture.FilePathRandomIntegersResults, SortingMethod.Selection);

        // assert
        var resultContents = File.ReadAllText(_fixture.FilePathRandomIntegersResults);
        var sampleContents = File.ReadAllText(_fixture.FilePathRandomIntegersSample);

        resultContents.Should().Match(sampleContents);
    }
    
    [Fact]
    public void Sort_Integers_MergeMethod_ResultFileCoincidesSample()
    {
        // arrange
        var sorter = new Sorter(new IntegerSortingFactory());

        // act
        sorter.Sort(_fixture.FilePathRandomIntegers,_fixture.FilePathRandomIntegersResults, SortingMethod.Merge);

        // assert
        var resultContents = File.ReadAllText(_fixture.FilePathRandomIntegersResults);
        var sampleContents = File.ReadAllText(_fixture.FilePathRandomIntegersSample);

        resultContents.Should().Match(sampleContents);
    }
    
    [Fact]
    public void Sort_Strings_InsertionMethod_ResultFileCoincidesSample()
    {
        // arrange
        var sorter = new Sorter(new StringSortingFactory());

        // act
        sorter.Sort(_fixture.FilePathRandomStrings, _fixture.FilePathRandomStringsResults, SortingMethod.Insertion);

        // assert
        var resultContents = File.ReadAllText(_fixture.FilePathRandomStringsResults);
        var sampleContents = File.ReadAllText(_fixture.FilePathRandomStringsSample);

        resultContents.Should().Match(sampleContents);
    }
    
    [Fact]
    public void Sort_Strings_SelectionMethod_ResultFileCoincidesSample()
    {
        // arrange
        var sorter = new Sorter(new StringSortingFactory());

        // act
        sorter.Sort(_fixture.FilePathRandomStrings,_fixture.FilePathRandomStringsResults, SortingMethod.Selection);

        // assert
        var resultContents = File.ReadAllText(_fixture.FilePathRandomStringsResults);
        var sampleContents = File.ReadAllText(_fixture.FilePathRandomStringsSample);

        resultContents.Should().Match(sampleContents);
    }
    
    [Fact]
    public void Sort_Strings_MergeMethod_ResultFileCoincidesSample()
    {
        // arrange
        var sorter = new Sorter(new StringSortingFactory());

        // act
        sorter.Sort(_fixture.FilePathRandomStrings, _fixture.FilePathRandomStringsResults, SortingMethod.Merge);

        // assert
        var resultContents = File.ReadAllText(_fixture.FilePathRandomStringsResults);
        var sampleContents = File.ReadAllText(_fixture.FilePathRandomStringsSample);

        resultContents.Should().Match(sampleContents);
    }
}
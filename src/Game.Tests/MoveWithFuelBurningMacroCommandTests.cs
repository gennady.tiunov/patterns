﻿using FluentAssertions;
using Game.Commands;
using Game.Movement;
using Moq;
using Xunit;

namespace Game.Tests;

public static class MoveWithFuelBurningMacroCommandTests
{
    [Fact]
    public static void Execute_EnoughFuel_ObjectMovesToNewPositionAndBurnsFuelCorrectly()
    {
        // arrange
        var objectPosition = new Vector(12, 5);
        var objectVelocity = new Vector(-7, 3);
        var expectedPosition = new Vector(5, 8);
        var movableMock = new Mock<IMovable>();
        movableMock
            .Setup(x => x.GetPosition())
            .Returns(objectPosition);
        movableMock
            .Setup(x => x.GetVelocity())
            .Returns(objectVelocity);
        var moveCommand = new MoveCommand(movableMock.Object);
        
        var burningVelocity = 2;
        var fuelLevel = 10;
        var expectedFuelRemains = fuelLevel - burningVelocity;
        var fuelPoweredMock = new Mock<IFuelPowered>();
        fuelPoweredMock
            .Setup(x => x.BurningVelocity)
            .Returns(burningVelocity);
        fuelPoweredMock
            .Setup(x => x.GetFuelLevel())
            .Returns(fuelLevel);
        var checkFuelCommand = new CheckFuelCommand(fuelPoweredMock.Object);
        var burnFuelCommand = new BurnFuelCommand(fuelPoweredMock.Object);
        
        var macroCommand = new MacroCommand(
            new ICommand [] {
                checkFuelCommand,
                moveCommand,
                burnFuelCommand
            });
        
        // act
        macroCommand.Execute();

        // assert
        movableMock.Verify(x => x.GetPosition(), Times.Once);
        movableMock.Verify(x => x.GetVelocity(), Times.Once);
        movableMock.Verify(x => x.SetPosition(expectedPosition), Times.Once);
        movableMock.VerifyNoOtherCalls();
        
        fuelPoweredMock.Verify(x => x.BurningVelocity, Times.Exactly(2));
        fuelPoweredMock.Verify(x => x.GetFuelLevel(), Times.Exactly(2));
        fuelPoweredMock.Verify(x => x.SetFuelLevel(expectedFuelRemains), Times.Once);
        fuelPoweredMock.VerifyNoOtherCalls();
    }
    
    [Fact]
    public static void Execute_NotEnoughFuel_CheckThrowsExceptionAndNoFurtherCommandsExecute()
    {
        // arrange
        var objectPosition = new Vector(12, 5);
        var objectVelocity = new Vector(-7, 3);
        var movableMock = new Mock<IMovable>();
        movableMock
            .Setup(x => x.GetPosition())
            .Returns(objectPosition);
        movableMock
            .Setup(x => x.GetVelocity())
            .Returns(objectVelocity);
        var moveCommand = new MoveCommand(movableMock.Object);
        
        var burningVelocity = 2;
        var fuelLevel = 1;
        var fuelPoweredMock = new Mock<IFuelPowered>();
        fuelPoweredMock
            .Setup(x => x.BurningVelocity)
            .Returns(burningVelocity);
        fuelPoweredMock
            .Setup(x => x.GetFuelLevel())
            .Returns(fuelLevel);
        var checkFuelCommand = new CheckFuelCommand(fuelPoweredMock.Object);
        var burnFuelCommand = new BurnFuelCommand(fuelPoweredMock.Object);
        
        var macroCommand = new MacroCommand(
            new ICommand [] {
                checkFuelCommand,
                moveCommand,
                burnFuelCommand
            });
        
        // act
        var action = () => macroCommand.Execute();;
        
        // assert
        action.Should().ThrowExactly<CommandException>();
        
        movableMock.VerifyNoOtherCalls();
        movableMock.Object.GetPosition().Should().Be(objectPosition);
        
        fuelPoweredMock.Verify(x => x.BurningVelocity, Times.Once);
        fuelPoweredMock.Verify(x => x.GetFuelLevel(), Times.Once);
        fuelPoweredMock.VerifyNoOtherCalls();
        fuelPoweredMock.Object.GetFuelLevel().Should().Be(fuelLevel);
   }
}
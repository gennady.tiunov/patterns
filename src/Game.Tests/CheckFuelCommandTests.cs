﻿using FluentAssertions;
using Game.Commands;
using Game.Movement;
using Moq;
using Xunit;

namespace Game.Tests;

public static class CheckFuelCommandTests
{
    [Fact]
    public static void Execute_EnoughFuel_CommandSucceeds()
    {
        // arrange
        var burningVelocity = 2;
        var fuelLevel = 10;

        var fuelPoweredMock = new Mock<IFuelPowered>();

        fuelPoweredMock
            .Setup(x => x.BurningVelocity)
            .Returns(burningVelocity);

        fuelPoweredMock
            .Setup(x => x.GetFuelLevel())
            .Returns(fuelLevel);

        var command = new CheckFuelCommand(fuelPoweredMock.Object);

        // act
        command.Execute();

        // assert
        fuelPoweredMock.Verify(x => x.BurningVelocity, Times.Once);
        fuelPoweredMock.Verify(x => x.GetFuelLevel(), Times.Once);
        fuelPoweredMock.VerifyNoOtherCalls();
    }
    
    [Fact]
    public static void Execute_NotEnoughFuel_CommandThrowsException()
    {
        // arrange
        var burningVelocity = 2;
        var fuelLevel = 1;

        var fuelPoweredMock = new Mock<IFuelPowered>();

        fuelPoweredMock
            .Setup(x => x.BurningVelocity)
            .Returns(burningVelocity);

        fuelPoweredMock
            .Setup(x => x.GetFuelLevel())
            .Returns(fuelLevel);

        var command = new CheckFuelCommand(fuelPoweredMock.Object);

        // act
        var action = () => command.Execute();

        // assert
        action.Should().ThrowExactly<CommandException>();
        fuelPoweredMock.Verify(x => x.BurningVelocity, Times.Once);
        fuelPoweredMock.Verify(x => x.GetFuelLevel(), Times.Once);
        fuelPoweredMock.VerifyNoOtherCalls();
    }
}

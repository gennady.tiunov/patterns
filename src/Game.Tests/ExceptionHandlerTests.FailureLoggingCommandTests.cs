﻿using FluentAssertions;
using Game.Commands;
using Game.ExceptionHandling;
using Game.Infrastructure;
using Xunit;

namespace Game.Tests;

public static partial class ExceptionHandlerTests
{
    [Fact]
    public static void RegisterFailureLoggingCommandForSpecificCommandAndException_HandleException_HandlerFound()
    {
        // arrange
        var commandQueue = new Queue<ICommand>();
        var exceptionHandler = new ExceptionHandler();
        var logger = new ConsoleLogger();
        
        var command = new EmptyCommand();
        var exception = new CommandException("Things happen");
        
        exceptionHandler.RegisterHandler<EmptyCommand, CommandException>((cmd, e) =>
        {
            commandQueue.Enqueue(new FailureLoggingCommand(cmd, e, logger));
        });
        
        // act
        exceptionHandler.HandleException(command, exception);

        // assert
        commandQueue.Should().HaveCount(1);
        
        var scheduledCommand = commandQueue.Peek();
        scheduledCommand.Should().BeOfType<FailureLoggingCommand>();
    }
    
    [Fact]
    public static void RegisterFailureLoggingCommandForSpecificCommand_HandleException_HandlerFound()
    {
        // arrange
        var commandQueue = new Queue<ICommand>();
        var exceptionHandler = new ExceptionHandler();
        var logger = new ConsoleLogger();
        
        var command = new EmptyCommand();
        var exception = new CommandException("Things happen");
        
        exceptionHandler.RegisterHandler<EmptyCommand>((cmd, e) =>
        {
            commandQueue.Enqueue(new FailureLoggingCommand(cmd, e, logger));
        });
        
        // act
        exceptionHandler.HandleException(command, exception);

        // assert
        commandQueue.Should().HaveCount(1);
        
        var scheduledCommand = commandQueue.Peek();
        scheduledCommand.Should().BeOfType<FailureLoggingCommand>();
    }
}

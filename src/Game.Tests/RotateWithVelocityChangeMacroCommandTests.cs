﻿using Game.Commands;
using Game.Movement;
using Moq;
using Xunit;

namespace Game.Tests;

public static class RotateWithVelocityChangeMacroCommandTests
{
    [Fact]
    public static void Execute_ObjectMovesAndRotates_ObjectRotatesAndVelocityChanges()
    {
        // arrange
        var objectVelocity = new Vector(3, 3);
        var movableMock = new Mock<IMovable>();
        movableMock
            .Setup(x => x.GetVelocity())
            .Returns(objectVelocity);
        
        var objectAngularDirection = 1;
        var objectAngularVelocity = 3;
        var angularDirectionsCount = 8;
        var expectedAngularDirection = 4;
        var rotableMock = new Mock<IRotable>();
        rotableMock
            .Setup(x => x.GetAngularDirection())
            .Returns(objectAngularDirection);
        rotableMock
            .Setup(x => x.GetAngularVelocity())
            .Returns(objectAngularVelocity);
        rotableMock
            .Setup(x => x.GetAngularDirectionsCount())
            .Returns(angularDirectionsCount);
        var rotateCommand = new RotateCommand(rotableMock.Object);

        var changeVelocityCommand = new ChangeVelocityCommand(movableMock.Object, rotableMock.Object);
        
        var macroCommand = new MacroCommand(
            new ICommand [] {
                rotateCommand,
                changeVelocityCommand
            });
        
        // act
        macroCommand.Execute();

        // assert
        movableMock.Verify(x => x.GetVelocity(), Times.Once);
        movableMock.Verify(x => x.SetVelocity(
            It.Is<Vector>(v => v != objectVelocity)),
            Times.Once);
        movableMock.VerifyNoOtherCalls();
        
        rotableMock.Verify(x => x.GetAngularDirection(), Times.Once);
        rotableMock.Verify(x => x.GetAngularVelocity(), Times.Exactly(2));
        rotableMock.Verify(x => x.GetAngularDirectionsCount(), Times.Exactly(2));
        rotableMock.Verify(x => x.SetAngularDirection(expectedAngularDirection), Times.Once);
        rotableMock.VerifyNoOtherCalls();
    }
    
    [Fact]
    public static void Execute_ObjectOnlyRotates_ObjectRotatesAndVelocityDoesNotChange()
    {
        // arrange
        var objectVelocity = new Vector(0, 0);
        var movableMock = new Mock<IMovable>();
        movableMock
            .Setup(x => x.GetVelocity())
            .Returns(objectVelocity);
        
        var objectAngularDirection = 1;
        var objectAngularVelocity = 3;
        var angularDirectionsCount = 8;
        var expectedAngularDirection = 4;
        var rotableMock = new Mock<IRotable>();
        rotableMock
            .Setup(x => x.GetAngularDirection())
            .Returns(objectAngularDirection);
        rotableMock
            .Setup(x => x.GetAngularVelocity())
            .Returns(objectAngularVelocity);
        rotableMock
            .Setup(x => x.GetAngularDirectionsCount())
            .Returns(angularDirectionsCount);
        var rotateCommand = new RotateCommand(rotableMock.Object);

        var changeVelocityCommand = new ChangeVelocityCommand(movableMock.Object, rotableMock.Object);
        
        var macroCommand = new MacroCommand(
            new ICommand [] {
                rotateCommand,
                changeVelocityCommand
            });
        
        // act
        macroCommand.Execute();

        // assert
        movableMock.Verify(x => x.GetVelocity(), Times.Once);
        movableMock.Verify(x => x.SetVelocity(
                It.Is<Vector>(v => v == objectVelocity)),
            Times.Once);
        movableMock.VerifyNoOtherCalls();
        
        rotableMock.Verify(x => x.GetAngularDirection(), Times.Once);
        rotableMock.Verify(x => x.GetAngularVelocity(), Times.Exactly(2));
        rotableMock.Verify(x => x.GetAngularDirectionsCount(), Times.Exactly(2));
        rotableMock.Verify(x => x.SetAngularDirection(expectedAngularDirection), Times.Once);
        rotableMock.VerifyNoOtherCalls();
    }
    
    [Fact]
    public static void Execute_ObjectOnlyMoves_ObjectDoesNotRotateAndVelocityDoesNotChange()
    {
        // arrange
        var objectVelocity = new Vector(3, 3);
        var movableMock = new Mock<IMovable>();
        movableMock
            .Setup(x => x.GetVelocity())
            .Returns(objectVelocity);
        
        var objectAngularDirection = 3;
        var objectAngularVelocity = 0;
        var angularDirectionsCount = 8;
        var rotableMock = new Mock<IRotable>();
        rotableMock
            .Setup(x => x.GetAngularDirection())
            .Returns(objectAngularDirection);
        rotableMock
            .Setup(x => x.GetAngularVelocity())
            .Returns(objectAngularVelocity);
        rotableMock
            .Setup(x => x.GetAngularDirectionsCount())
            .Returns(angularDirectionsCount);
        var rotateCommand = new RotateCommand(rotableMock.Object);

        var changeVelocityCommand = new ChangeVelocityCommand(movableMock.Object, rotableMock.Object);
        
        var macroCommand = new MacroCommand(
            new ICommand [] {
                rotateCommand,
                changeVelocityCommand
            });
        
        // act
        macroCommand.Execute();

        // assert
        movableMock.Verify(x => x.GetVelocity(), Times.Once);
        movableMock.Verify(x => x.SetVelocity(
                It.Is<Vector>(v => v == objectVelocity)),
            Times.Once);
        movableMock.VerifyNoOtherCalls();
        
        rotableMock.Verify(x => x.GetAngularDirection(), Times.Once);
        rotableMock.Verify(x => x.GetAngularVelocity(), Times.Exactly(2));
        rotableMock.Verify(x => x.GetAngularDirectionsCount(), Times.Exactly(2));
        rotableMock.Verify(x => x.SetAngularDirection(objectAngularDirection), Times.Once);
        rotableMock.VerifyNoOtherCalls();
    }
}
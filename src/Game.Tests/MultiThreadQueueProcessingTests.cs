using System.Collections.Concurrent;
using FluentAssertions;
using Game.Commands;
using Game.Infrastructure;
using Xunit;

namespace Game.Tests;

public static class MultiThreadQueueProcessingTests
{
    [Fact]
    public static void StartThreadProcessingCommand_Executes_ThreadStarts()
    {
        // arrange
        var queue = new BlockingCollection<ICommand>();

        // thread to handle StartThreadProcessingCommand
        var mainThread = new Thread(ListenToQueueInServerThread);
        var mainServerThread = new ServerThread(mainThread, queue, new ConsoleLogger());
        mainServerThread.Start();

        // thread to be started by StartThreadProcessingCommand
        var childThread = new Thread(ListenToQueueInServerThread);
        var childServerThread = new ServerThread(childThread, queue, new ConsoleLogger());
        var startChildThreadCommand = new StartThreadProcessingCommand(childServerThread);

        var @event = new AutoResetEvent(false);

        var macroCommand = new MacroCommand(
            new ICommand[]
            {
                startChildThreadCommand,
                new ActionCommand(() => @event.Set())
            });

        childServerThread.IsRunning.Should().BeFalse();

        // act
        queue.Add(macroCommand);

        // wait till command has been executed
        @event.WaitOne();

        // assert
        childServerThread.IsRunning.Should().BeTrue();

        // stop threads
        childServerThread.HardStop();
        mainServerThread.HardStop();
    }

    [Fact]
    public static void HardStopThreadProcessingCommand_Executes_ThreadFinishes()
    {
        // arrange
        var queue = new BlockingCollection<ICommand>();

        // thread to handle StartThreadProcessingCommand
        var mainThread = new Thread(ListenToQueueInServerThread);
        var mainServerThread = new ServerThread(mainThread, queue, new ConsoleLogger());
        mainServerThread.Start();

        // thread to be started by StartThreadProcessingCommand
        // and stopped by HardStopThreadProcessingCommand
        var childThread = new Thread(ListenToQueueInServerThread);
        var childServerThread = new ServerThread(childThread, queue, new ConsoleLogger());
        var startChildThreadCommand = new StartThreadProcessingCommand(childServerThread);
        var hardStopChildThreadCommand = new HardStopThreadProcessingCommand(childServerThread);

        var @event = new AutoResetEvent(false);

        var startMacroCommand = new MacroCommand(
            new ICommand[]
            {
                startChildThreadCommand,
                new ActionCommand(() => @event.Set())
            });

        childServerThread.IsRunning.Should().BeFalse();

        queue.Add(startMacroCommand);

        @event.WaitOne();

        childServerThread.IsRunning.Should().BeTrue();

        // stop the thread so that it does not compete for the queue
        mainServerThread.HardStop();
        do { } while (mainServerThread.IsRunning);

        var hardStopMacroCommand = new MacroCommand(
            new ICommand[]
            {
                new ActionCommand(() => queue.Add(new EmptyCommand())),
                new ActionCommand(() => queue.Add(new EmptyCommand())),
                new ActionCommand(() => queue.Add(new EmptyCommand())),
                hardStopChildThreadCommand,
                new ActionCommand(() => queue.Add(new EmptyCommand())),
                new ActionCommand(() => queue.Add(new EmptyCommand())),
                new ActionCommand(() => queue.Add(new EmptyCommand())),
                new ActionCommand(() => @event.Set()),
            });

        // act
        queue.Add(hardStopMacroCommand);
        queue.Add(new EmptyCommand());
        queue.Add(new EmptyCommand());
        queue.Add(new EmptyCommand());

        // wait till the command has been executed
        @event.WaitOne();

        // wait till the thread has truly been finished (as hard stop only changes behaviour while not stopping it immidiately)
        do { } while (childServerThread.IsRunning);

        // assert
        queue.Should().NotBeEmpty();
    }

    [Fact]
    public static void SoftStopThreadProcessingCommand_Executes_ThreadFinishesWhenQueueIsEmpty()
    {
        // arrange
        var queue = new BlockingCollection<ICommand>();

        // thread to handle StartThreadProcessingCommand
        var mainThread = new Thread(ListenToQueueInServerThread);
        var mainServerThread = new ServerThread(mainThread, queue, new ConsoleLogger());
        mainServerThread.Start();

        // thread to be started by StartThreadProcessingCommand
        // and stopped by SoftStopThreadProcessingCommand
        var childThread = new Thread(ListenToQueueInServerThread);
        var childServerThread = new ServerThread(childThread, queue, new ConsoleLogger());
        var startChildThreadCommand = new StartThreadProcessingCommand(childServerThread);
        var softStopChildThreadCommand = new SoftStopThreadProcessingCommand(childServerThread);

        var @event = new AutoResetEvent(false);

        var startMacroCommand = new MacroCommand(
            new ICommand[]
            {
                startChildThreadCommand,
                new ActionCommand(() => @event.Set())
            });

        childServerThread.IsRunning.Should().BeFalse();

        queue.Add(startMacroCommand);

        @event.WaitOne();

        childServerThread.IsRunning.Should().BeTrue();

        // stop the thread so that it does not compete for the queue
        mainServerThread.HardStop();
        do { } while (mainServerThread.IsRunning);

        var softStopMacroCommand = new MacroCommand(
            new ICommand[]
            {
                new ActionCommand(() => queue.Add(new EmptyCommand())),
                new ActionCommand(() => queue.Add(new EmptyCommand())),
                new ActionCommand(() => queue.Add(new EmptyCommand())),
                softStopChildThreadCommand,
                new ActionCommand(() => @event.Set()),
                new ActionCommand(() => queue.Add(new EmptyCommand())),
                new ActionCommand(() => queue.Add(new EmptyCommand())),
                new ActionCommand(() => queue.Add(new EmptyCommand())),
            });

        queue.Add(softStopMacroCommand);
        queue.Add(new EmptyCommand());
        queue.Add(new EmptyCommand());
        queue.Add(new EmptyCommand());

        // wait till the command has been executed
        @event.WaitOne();

        // wait till the thread has been finished
        do { } while (childServerThread.IsRunning);

        // assert
        queue.Should().BeEmpty();
    }

    private static void ListenToQueueInServerThread(object? parameters)
    {
        var serverThread = (ServerThread) parameters!;
        while (!serverThread.CheckStoppingBehaviour())
        {
            if (serverThread.CommandQueue.TryTake(out var command))
            {
                try {  command.Execute(); }
                catch {}
            }
        }
    }
}

﻿using FluentAssertions;
using Game.Commands;
using Game.ExceptionHandling;
using Game.Infrastructure;
using Xunit;

namespace Game.Tests;

public static partial class ExceptionHandlerTests
{
    [Fact]
    public static void RegisterNoHandlers_HandleException_InvalidOperationExceptionRaised()
    {
        // arrange
        var exceptionHandler = new ExceptionHandler();
        var command = new EmptyCommand();
        var exception = new CommandException("Things happen");
        
        // act
        var action = () => exceptionHandler.HandleException(command, exception);

        // assert
        action.Should().ThrowExactly<InvalidOperationException>();
    }

    [Fact]
    public static void RegisterUnsuitableHandlers_HandleException_InvalidOperationExceptionRaised()
    {
        // arrange
        var commandQueue = new Queue<ICommand>();
        var exceptionHandler = new ExceptionHandler();
        var logger = new ConsoleLogger();
        
        var command = new EmptyCommand();
        var exception = new CommandException("Things happen");
        
        exceptionHandler.RegisterHandler<EmptyCommand, Exception>((cmd, e) =>
        {
            commandQueue.Enqueue(new FailureLoggingCommand(cmd, e, logger));
        });
        exceptionHandler.RegisterHandler<BurnFuelCommand, CommandException>((cmd, e) =>
        {
            commandQueue.Enqueue(new FailureLoggingCommand(cmd, e, logger));
        });
        exceptionHandler.RegisterHandler<BurnFuelCommand>((cmd, e) =>
        {
            commandQueue.Enqueue(new FailureLoggingCommand(cmd, e, logger));
        });
        
        // act
        var action = () => exceptionHandler.HandleException(command, exception);

        // assert
        action.Should().ThrowExactly<InvalidOperationException>();
        commandQueue.Should().BeEmpty();
    }
}

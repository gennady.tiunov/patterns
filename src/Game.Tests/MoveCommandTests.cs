﻿using FluentAssertions;
using Game.Commands;
using Game.Movement;
using Moq;
using Xunit;

namespace Game.Tests;

public static class MoveCommandTests
{
    [Fact]
    public static void Execute_CorrectMovableObject_MovesToNewPosition()
    {
        // arrange
        var objectPosition = new Vector(12, 5);
        var objectVelocity = new Vector(-7, 3);
        var expectedPosition = new Vector(5, 8);
        
        var movableMock = new Mock<IMovable>();
        
        movableMock
            .Setup(x => x.GetPosition())
            .Returns(objectPosition);
        
        movableMock
            .Setup(x => x.GetVelocity())
            .Returns(objectVelocity);
        
        var command = new MoveCommand(movableMock.Object);
        
        // act
        command.Execute();

        // assert
        movableMock.Verify(x => x.GetPosition(), Times.Once);
        movableMock.Verify(x => x.GetVelocity(), Times.Once);
        movableMock.Verify(x => x.SetPosition(expectedPosition), Times.Once);
        movableMock.VerifyNoOtherCalls();
    }
    
    [Fact]
    public static void Execute_MovableGetPositionThrowsException_CommandRethrowsException()
    {
        // arrange
        var movableMock = new Mock<IMovable>();
        
        movableMock
            .Setup(x => x.GetPosition())
            .Throws(new InvalidOperationException());
        
        var command = new MoveCommand(movableMock.Object);
        
        // act
        var action = () => command.Execute();
        
        // assert
        action.Should().ThrowExactly<InvalidOperationException>();
        movableMock.Verify(x => x.GetPosition(), Times.Once);
        movableMock.VerifyNoOtherCalls();
    }
    
    [Fact]
    public static void Execute_MovableGetVelocityThrowsException_CommandRethrowsException()
    {
        // arrange
        var objectPosition = new Vector(
            new Random().Next(0, int.MaxValue), 
            new Random().Next(0, int.MaxValue));
        
        var movableMock = new Mock<IMovable>();
        
        movableMock
            .Setup(x => x.GetPosition())
            .Returns(objectPosition);
        
        movableMock
            .Setup(x => x.GetVelocity())
            .Throws(new InvalidOperationException());
        
        var command = new MoveCommand(movableMock.Object);
        
        // act
        var action = () => command.Execute();
        
        // assert
        action.Should().ThrowExactly<InvalidOperationException>();
        movableMock.Verify(s => s.GetPosition(), Times.Once);
        movableMock.Verify(s => s.GetVelocity(), Times.Once);
        movableMock.VerifyNoOtherCalls();
    }
    
    [Fact]
    public static void Execute_MovableSetPositionThrowsException_CommandRethrowsException()
    {
        // arrange
        var objectPosition = new Vector(
            new Random().Next(0, int.MaxValue), 
            new Random().Next(0, int.MaxValue));
        
        var objectVelocity = new Vector(
            new Random().Next(0, int.MaxValue), 
            new Random().Next(0, int.MaxValue));

        var expectedPosition = Vector.Plus(objectPosition, objectVelocity);
        
        var movableMock = new Mock<IMovable>();
        
        movableMock
            .Setup(x => x.GetPosition())
            .Returns(objectPosition);
        
        movableMock
            .Setup(x => x.GetVelocity())
            .Returns(objectVelocity);
        
        movableMock
            .Setup(x => x.SetPosition(It.IsAny<Vector>()))
            .Throws(new InvalidOperationException());
        
        var command = new MoveCommand(movableMock.Object);
        
        // act
        var action = () => command.Execute();
        
        // assert
        action.Should().ThrowExactly<InvalidOperationException>();
        movableMock.Verify(s => s.GetPosition(), Times.Once);
        movableMock.Verify(s => s.GetVelocity(), Times.Once);
        movableMock.Verify(s => s.SetPosition(expectedPosition), Times.Once);
        movableMock.VerifyNoOtherCalls();
    }
}
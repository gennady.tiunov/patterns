﻿using Game.Commands;
using Game.Movement;
using Moq;
using Xunit;

namespace Game.Tests;

public static class BurnFuelCommandTests
{
    [Fact]
    public static void Execute_FuelDecreasesCorrectly()
    {
        // arrange
        var burningVelocity = 2;
        var fuelLevel = 10;
        var expectedFuelRemains = fuelLevel - burningVelocity;

        var fuelPoweredMock = new Mock<IFuelPowered>();

        fuelPoweredMock
            .Setup(x => x.BurningVelocity)
            .Returns(burningVelocity);

        fuelPoweredMock
            .Setup(x => x.GetFuelLevel())
            .Returns(fuelLevel);

        var command = new BurnFuelCommand(fuelPoweredMock.Object);

        // act
        command.Execute();

        // assert
        fuelPoweredMock.Verify(x => x.BurningVelocity, Times.Once);
        fuelPoweredMock.Verify(x => x.GetFuelLevel(), Times.Once);
        fuelPoweredMock.Verify(x => x.SetFuelLevel(expectedFuelRemains), Times.Once);
        fuelPoweredMock.VerifyNoOtherCalls();
    }
}

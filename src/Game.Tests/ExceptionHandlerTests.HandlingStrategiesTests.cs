﻿using FluentAssertions;
using Game.Commands;
using Game.ExceptionHandling;
using Game.Infrastructure;
using Xunit;

namespace Game.Tests;

public static partial class ExceptionHandlerTests
{
    [Fact]
    public static void SingleRetryAndLoggingStrategy_HandleExceptions_RelevantCommandsScheduled()
    {
        // arrange
        var commandQueue = new Queue<ICommand>();
        var exceptionHandler = new ExceptionHandler();
        var logger = new ConsoleLogger();
        
        exceptionHandler.RegisterHandler<EmptyCommand, CommandException>((cmd, e) =>
        {
            commandQueue.Enqueue(new FirstRetryCommand(cmd, logger));
        });
        exceptionHandler.RegisterHandler<FirstRetryCommand, CommandException>((cmd, e) =>
        {
            commandQueue.Enqueue(new FailureLoggingCommand(cmd, e, logger));
        });
        
        // act
        exceptionHandler.HandleException(new EmptyCommand(), new CommandException());
        exceptionHandler.HandleException(new FirstRetryCommand(new EmptyCommand(), logger), new CommandException());

        // assert
        commandQueue.Should().HaveCount(2);
        
        var firstScheduledCommand = commandQueue.Dequeue();
        firstScheduledCommand.Should().BeOfType<FirstRetryCommand>();
        var secondScheduledCommand = commandQueue.Dequeue();
        secondScheduledCommand.Should().BeOfType<FailureLoggingCommand>();
    }
    
    [Fact]
    public static void DoubleRetryAndLoggingStrategy_HandleException_RelevantCommandsScheduled()
    {
        // arrange
        var commandQueue = new Queue<ICommand>();
        var exceptionHandler = new ExceptionHandler();
        var logger = new ConsoleLogger();
        
        exceptionHandler.RegisterHandler<EmptyCommand, CommandException>((cmd, e) =>
        {
            commandQueue.Enqueue(new FirstRetryCommand(cmd, logger));
        });
        exceptionHandler.RegisterHandler<FirstRetryCommand, CommandException>((cmd, e) =>
        {
            commandQueue.Enqueue(new SecondRetryCommand(((FirstRetryCommand)cmd).InnerCommand, logger));
        });
        exceptionHandler.RegisterHandler<SecondRetryCommand, CommandException>((cmd, e) =>
        {
            commandQueue.Enqueue(new FailureLoggingCommand(cmd, e, logger));
        });
        
        // act
        exceptionHandler.HandleException(new EmptyCommand(), new CommandException());
        exceptionHandler.HandleException(new FirstRetryCommand(new EmptyCommand(), logger), new CommandException());
        exceptionHandler.HandleException(new SecondRetryCommand(new EmptyCommand(), logger), new CommandException());

        // assert
        commandQueue.Should().HaveCount(3);
        
        var firstScheduledCommand = commandQueue.Dequeue();
        firstScheduledCommand.Should().BeOfType<FirstRetryCommand>();
        var secondScheduledCommand = commandQueue.Dequeue();
        secondScheduledCommand.Should().BeOfType<SecondRetryCommand>();
        var thirdScheduledCommand = commandQueue.Dequeue();
        thirdScheduledCommand.Should().BeOfType<FailureLoggingCommand>();
    }
}

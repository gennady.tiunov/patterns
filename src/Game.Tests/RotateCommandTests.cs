﻿using FluentAssertions;
using Game.Commands;
using Game.Movement;
using Moq;
using Xunit;

namespace Game.Tests;

public static class RotateCommandTests
{
    [Fact]
    public static void Execute_CorrectRotableObject_RotatesToNewDirection()
    {
        // arrange
        var objectAngularDirection = 1;
        var objectAngularVelocity = 3;
        var angularDirectionsCount = 8;

        var expectedAngularDirection = 4;
        
        var rotableMock = new Mock<IRotable>();
        
        rotableMock
            .Setup(x => x.GetAngularDirection())
            .Returns(objectAngularDirection);
        
        rotableMock
            .Setup(x => x.GetAngularVelocity())
            .Returns(objectAngularVelocity);
        
        rotableMock
            .Setup(x => x.GetAngularDirectionsCount())
            .Returns(angularDirectionsCount);
        
        var command = new RotateCommand(rotableMock.Object);
        
        // act
        command.Execute();

        // assert
        rotableMock.Verify(x => x.GetAngularDirection(), Times.Once);
        rotableMock.Verify(x => x.GetAngularVelocity(), Times.Once);
        rotableMock.Verify(x => x.GetAngularDirectionsCount(), Times.Once);
        rotableMock.Verify(x => x.SetAngularDirection(expectedAngularDirection), Times.Once);
        rotableMock.VerifyNoOtherCalls();
    }
    
    [Fact]
    public static void Execute_RotableGetAngularDirectionThrowsException_CommandRethrowsException()
    {
        // arrange
        var rotableMock = new Mock<IRotable>();
        
        rotableMock
            .Setup(x => x.GetAngularDirection())
            .Throws(new InvalidOperationException());
        
        var command = new RotateCommand(rotableMock.Object);
        
        // act
        var action = () => command.Execute();
        
        // assert
        action.Should().ThrowExactly<InvalidOperationException>();
        rotableMock.Verify(x => x.GetAngularDirection(), Times.Once);
        rotableMock.VerifyNoOtherCalls();
    }
    
    [Fact]
    public static void Execute_RotableGetAngularVelocityThrowsException_CommandRethrowsException()
    {
        // arrange
        var objectAngularDirection = new Random().Next(0, int.MaxValue);
        
        var rotableMock = new Mock<IRotable>();
        
        rotableMock
            .Setup(x => x.GetAngularDirection())
            .Returns(objectAngularDirection);
        
        rotableMock
            .Setup(x => x.GetAngularVelocity())
            .Throws(new InvalidOperationException());
        
        var command = new RotateCommand(rotableMock.Object);
        
        // act
        var action = () => command.Execute();
        
        // assert
        action.Should().ThrowExactly<InvalidOperationException>();
        rotableMock.Verify(x => x.GetAngularDirection(), Times.Once);
        rotableMock.Verify(x => x.GetAngularVelocity(), Times.Once);
        rotableMock.VerifyNoOtherCalls();
    }
    
    [Fact]
    public static void Execute_RotableGetAngularDirectionsCountThrowsException_CommandRethrowsException()
    {
        // arrange
        var objectAngularDirection = new Random().Next(0, int.MaxValue);
        var objectAngularVelocity = new Random().Next(0, int.MaxValue);
        
        var rotableMock = new Mock<IRotable>();
        
        rotableMock
            .Setup(x => x.GetAngularDirection())
            .Returns(objectAngularDirection);
        
        rotableMock
            .Setup(x => x.GetAngularVelocity())
            .Returns(objectAngularVelocity);
        
        rotableMock
            .Setup(x => x.GetAngularDirectionsCount())
            .Throws(new InvalidOperationException());
        
        var command = new RotateCommand(rotableMock.Object);
        
        // act
        var action = () => command.Execute();
        
        // assert
        action.Should().ThrowExactly<InvalidOperationException>();
        rotableMock.Verify(x => x.GetAngularDirection(), Times.Once);
        rotableMock.Verify(x => x.GetAngularVelocity(), Times.Once);
        rotableMock.Verify(x => x.GetAngularDirectionsCount(), Times.Once);
        rotableMock.VerifyNoOtherCalls();
    }
    
    [Fact]
    public static void Execute_RotableSetAngularDirectionThrowsException_CommandRethrowsException()
    {
        // arrange
        var objectAngularDirection = new Random().Next(0, int.MaxValue);
        var objectAngularVelocity = new Random().Next(0, int.MaxValue);
        var angularDirectionsCount = new Random().Next(1, int.MaxValue);

        var expectedDirection = (objectAngularDirection + objectAngularVelocity) % angularDirectionsCount;
        
        var rotableMock = new Mock<IRotable>();
        
        rotableMock
            .Setup(x => x.GetAngularDirection())
            .Returns(objectAngularDirection);
        
        rotableMock
            .Setup(x => x.GetAngularVelocity())
            .Returns(objectAngularVelocity);
        
        rotableMock
            .Setup(x => x.GetAngularDirectionsCount())
            .Returns(angularDirectionsCount);
        
        rotableMock
            .Setup(x => x.SetAngularDirection(It.IsAny<int>()))
            .Throws(new InvalidOperationException());
        
        var command = new RotateCommand(rotableMock.Object);
        
        // act
        var action = () => command.Execute();
        
        // assert
        action.Should().ThrowExactly<InvalidOperationException>();
        rotableMock.Verify(x => x.GetAngularDirection(), Times.Once);
        rotableMock.Verify(x => x.GetAngularVelocity(), Times.Once);
        rotableMock.Verify(x => x.GetAngularDirectionsCount(), Times.Once);
        rotableMock.Verify(x => x.SetAngularDirection(expectedDirection), Times.Once);
        rotableMock.VerifyNoOtherCalls();
    }
}
﻿using FluentAssertions;
using Game.Commands;
using Game.ExceptionHandling;
using Game.Infrastructure;
using Moq;
using Xunit;

namespace Game.Tests;

public static class RetryCommandTests
{
    [Fact]
    public static void Execute_OriginalCommandExecutesCorrectly_RetryCommandNotScheduled()
    {
        // arrange
        var commandQueue = new Queue<ICommand>();
        var maxAttempts = 1;
        var loggerMock = new Mock<IGameLogger>();
        var originalCommandMock = new Mock<ICommand>();
        
        var retryCommand = new ExactCountRetryCommand(originalCommandMock.Object, maxAttempts, commandQueue, loggerMock.Object);

        // act
        retryCommand.Execute();

        // assert
        originalCommandMock.Verify(x => x.Execute(), Times.Once);
        commandQueue.Should().BeEmpty();
    }
    
    [Fact]
    public static void Execute_OriginalCommandFailsAndMaxAttemptsNotAchieved_RetryCommandScheduled()
    {
        // arrange
        var commandQueue = new Queue<ICommand>();
        var maxAttempts = 2;
        var loggerMock = new Mock<IGameLogger>();
        var originalCommandMock = new Mock<ICommand>();
        originalCommandMock
            .Setup(x => x.Execute())
            .Throws<Exception>();
        
        var retryCommand = new ExactCountRetryCommand(originalCommandMock.Object, maxAttempts, commandQueue, loggerMock.Object);

        // act
        retryCommand.Execute();

        // assert
        originalCommandMock.Verify(x => x.Execute(), Times.Once);
        commandQueue.Should().HaveCount(1);
        commandQueue.Peek().Should().Be(retryCommand);
    }
    
    [Fact]
    public static void Execute_OriginalCommandFailsAndMaxAttemptsAchieved_FailureLoggingCommandScheduled()
    {
        // arrange
        var commandQueue = new Queue<ICommand>();
        var maxAttempts = 1;
        var loggerMock = new Mock<IGameLogger>();
        var originalCommandMock = new Mock<ICommand>();
        originalCommandMock
            .Setup(x => x.Execute())
            .Throws<CommandException>();
        
        var retryCommand = new ExactCountRetryCommand(originalCommandMock.Object, maxAttempts, commandQueue, loggerMock.Object);

        // act
        retryCommand.Execute();

        // assert
        originalCommandMock.Verify(x => x.Execute(), Times.Once);
        commandQueue.Should().HaveCount(1);

        var scheduledCommand = commandQueue.Peek();
        scheduledCommand.Should().BeOfType<FailureLoggingCommand>();
    }
}

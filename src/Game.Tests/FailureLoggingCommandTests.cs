﻿using Game.Commands;
using Game.ExceptionHandling;
using Game.Infrastructure;
using Moq;
using Xunit;

namespace Game.Tests;

public static class FailureLoggingCommandTests
{
    [Fact]
    public static void Execute_CorrectMessageIsWrittenToLog()
    {
        // arrange
        var loggerMock = new Mock<IGameLogger>();
        var originalCommand = new EmptyCommand();
        var exception = new CommandException("Things happen");

        var expectedLogMessage =
            $"Exception '{exception.GetType().Name}' occured while executing command '{originalCommand.GetType().Name}': '{exception.Message}'"; 

        var failureLoggingCommand = new FailureLoggingCommand(originalCommand, exception, loggerMock.Object);

        // act
        failureLoggingCommand.Execute();

        // assert
        loggerMock.Verify(x => x
                .Log(It.Is<string>(message => message.Contains(expectedLogMessage))),
            Times.Once);
    }
}

﻿using Game;
using Game.Commands;
using Game.ExceptionHandling;
using Game.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Information()
    .Enrich.FromLogContext()
    .WriteTo.File("AppLog.log")
    .WriteTo.Console()
    .MinimumLevel.Debug()
    .CreateLogger();

var hostBuilder = Host
    .CreateDefaultBuilder(args)
    .ConfigureLogging(
        logging =>
        {
            logging.ClearProviders();
            logging.AddSerilog();
        })
    .ConfigureServices((h,services) =>
    {
        services.AddSingleton<Program>();
    });

var host = hostBuilder.Build();
host.Services.GetRequiredService<Program>().Run(args);

public partial class Program
{
    private readonly ILogger<Program> _logger;

    public Program(
        ILogger<Program> logger)
    {
         _logger = logger;
    }

    private void Run(string[] args)
    {
        var exceptionHandler = new ExceptionHandler();
        var logger = new ConsoleLogger();

        var server = new SingleThreadGameServer(exceptionHandler, logger);

        exceptionHandler.RegisterHandler<ExceptionCommand>((command, exception) =>
        {
            server.CommandQueue.Enqueue(new FailureLoggingCommand(command, exception, logger));
        });

        exceptionHandler.RegisterHandler<ExceptionCommand, TimeoutException>((command, exception) =>
        {
            server.CommandQueue.Enqueue(new FirstRetryCommand(command, logger));
        });

        exceptionHandler.RegisterHandler<ExceptionCommand, CommandException>((command, exception) =>
        {
            server.CommandQueue.Enqueue(new FirstRetryCommand(command, logger));
        });

        exceptionHandler.RegisterHandler<FirstRetryCommand>((command, exception) =>
        {
            server.CommandQueue.Enqueue(new FailureLoggingCommand(command, exception, logger));
        });

        exceptionHandler.RegisterHandler<FirstRetryCommand, CommandException>((command, exception) =>
        {
            server.CommandQueue.Enqueue(new SecondRetryCommand(((FirstRetryCommand)command).InnerCommand, logger));
        });

        exceptionHandler.RegisterHandler<SecondRetryCommand>((command, exception) =>
        {
            server.CommandQueue.Enqueue(new FailureLoggingCommand(command, exception, logger));
        });

        //server.ScheduleCommand(new EmptyCommand());
        //server.ScheduleCommand(new ExceptionCommand(new ArgumentException("Contract violation")));
        //server.ScheduleCommand(new ExceptionCommand(new TimeoutException("Try again later")));
        server.ScheduleCommand(new ExceptionCommand(new CommandException("Temporary server error")));

        server.Run();
    }
}
